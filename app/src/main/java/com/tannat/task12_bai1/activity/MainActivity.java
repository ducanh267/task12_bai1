package com.tannat.task12_bai1.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tannat.task12_bai1.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_CODE = 1;
    private static final String TAG = MainActivity.class.getName();
    private TelephonyManager mTelephonyManager;
    TextView mtv_call;
    EditText medt_phone_number;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "onReceive...", null);
            if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
                String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
                Toast.makeText(context, "Out going call: " + number, Toast.LENGTH_LONG).show();
            } else if(intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
                Toast.makeText(context, "Screen is on", Toast.LENGTH_LONG).show();
            }else if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
                Toast.makeText(context, "có tin nhắn đến", Toast.LENGTH_LONG).show();
            }
        }
    };
    PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);

            switch (state) {
                //trường hợp khi chưa có cuộc gội
//                case TelephonyManager.CALL_STATE_IDLE:
//                    Toast.makeText(MainActivity.this, "đang k có ai gọi", Toast.LENGTH_SHORT).show();
//                    break;
                //trường hợp có cuộc gọi đến
                case TelephonyManager.CALL_STATE_RINGING:
                    Toast.makeText(MainActivity.this, incomingNumber + "đang gọi", Toast.LENGTH_SHORT).show();
                    break;
                //trường hợp nghe cuộc gọi
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Toast.makeText(MainActivity.this, "đang nghe" + incomingNumber + "gọi" , Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
    }
    @Override
    protected void onStop() {
        super.onStop();
        mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initData();
    }

    private void initView() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.PROCESS_OUTGOING_CALLS, Manifest.permission.RECEIVE_SMS},REQUEST_CODE);

        medt_phone_number = findViewById(R.id.edt_phone_number);
        findViewById(R.id.tv_call).setOnClickListener(this);
    }

    private void initData() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_NEW_OUTGOING_CALL);
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == REQUEST_CODE){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Toast.makeText(this, "tạm biệt", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_call:
                String phoneNo = medt_phone_number.getText().toString();
                if(!phoneNo.isEmpty()) {
                    String dial = "tel:" + phoneNo;
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
                }else {
                    Toast.makeText(MainActivity.this, "hãy nhập số", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    public void processReceive(Context context, Intent intent) {
        //Thông báo khi có tin nhắn mới
        Toast.makeText(context, "Có 1 tin nhắn mới", Toast.LENGTH_LONG).show();

        TextView txtContent = (TextView) findViewById(R.id.tv_mess);
        //pdus để lấy gói tin nhắn
        String sms_extra = "pdus";
        Bundle bundle = intent.getExtras();
        //bundle trả về tập các tin nhắn gửi về cùng lúc
        Object[] objArr = (Object[]) bundle.get(sms_extra);
        String sms = "";
        //duyệt vòng lặp để đọc từng tin nhắn
        for (int i = 0; i < objArr.length; i++) {
            //lệnh chuyển đổi về tin nhắn createFromPdu
            SmsMessage smsMsg = SmsMessage.
                    createFromPdu((byte[]) objArr[i]);
            //lấy nội dung tin nhắn
            String body = smsMsg.getMessageBody();
            //lấy số điện thoại tin nhắn
            String phone = smsMsg.getDisplayOriginatingAddress();
            sms += phone + ":\n" + body + "\n";
            sms += "được gửi vào lúc";
            sms += getCurrentTime();
            //sms += java.time.LocalDateTime.now();
        }
        //hiển thị lên giao diện
        txtContent.setText(sms);
        Toast.makeText(this, sms, Toast.LENGTH_LONG).show();
    }
    //lấy ra thời gian lúc nhận tin nhắn
    private String getCurrentTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HHmmss");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    protected void onDestroy() {
        super.onDestroy();
        //hủy bỏ đăng ký khi tắt ứng dụng
        unregisterReceiver(receiver);
    }
}
